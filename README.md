# Stream conda recipe

Home: https://github.com/paulscherrerinstitute/StreamDevice

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS Driver for message based I/O
